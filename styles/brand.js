import * as material from 'material-colors'

export const colors = {
    Black: '#0A0A0A',
    White: '#FEFEFE',
    Primary: material.purple['900'],
    Secondary: material.pink['500'],
    Accent: material.blue['300']
}