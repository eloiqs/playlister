require('dotenv').config()
const querystring = require('querystring')
const cookieParser = require('cookie-parser')
const SpotifyWebApi = require('spotify-web-api-node');
const morgan = require('morgan')
const express = require('express')
const next = require('next')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const clientId = process.env.CLIENT_ID
const clientSecret = process.env.CLIENT_SECRET
const redirectUri = process.env.REDIRECT_URI

const spotifyApi = new SpotifyWebApi({ clientId, clientSecret, redirectUri })

const generateRandomString = (length) => {
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  let text = ''

  for (let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length))
  }
  return text
}

const stateKey = 'spotify_auth_state'


app.prepare()
.then(() => {
  const server = express()
  server.use(morgan('dev'))
  server.use(cookieParser())

  server.get('/login', (req, res) => {
    const scopes = ['user-read-private', 'user-library-read', 'playlist-modify-public', 'playlist-modify-private', 'playlist-read-private']
    const state = generateRandomString(16)
    res.cookie(stateKey, state)
    res.redirect(spotifyApi.createAuthorizeURL(scopes, state))
  })

  server.get('/logout', (req, res) => {
    const scopes = ['user-read-private', 'user-read-email']
    const state = generateRandomString(16)
    res.cookie(stateKey, state)
    const authorizationUrl = spotifyApi.createAuthorizeURL(scopes, state) + '&show_dialog=true'
    res.redirect(authorizationUrl)
  })

  server.get('/callback', async (req, res) => {
    const { code } = req.query;
    try {
      const data = await spotifyApi.authorizationCodeGrant(code)
      const { expires_in, access_token, refresh_token } = data.body;
      spotifyApi.setAccessToken(access_token);
      spotifyApi.setRefreshToken(refresh_token);
      const query = querystring.stringify({ access_token, refresh_token })
      res.redirect(`/user?${query}`);
    } catch (err) {
      res.redirect('/?error=invalid_token');
    }
  })

  server.get('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(3000, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:3000')
  })
})