import glamorous from 'glamorous'
import { colors } from '../styles/brand';

export default glamorous.h1({ 
    fontSize: '4rem', 
    color: colors.Primary, 
    fontFamily: 'Nunito' 
})