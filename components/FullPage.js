import glamorous from 'glamorous'
import { colors } from '../styles/brand';

export default glamorous.div({
  fontFamily: 'Nunito',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'column',
  height: '100vh',
  backgroundColor: colors.White
})