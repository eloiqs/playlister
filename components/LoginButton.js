import glamorous from 'glamorous'
import { colors } from '../styles/brand'

export default glamorous.button({
  fontFamily: 'Nunito',
  fontSize: '1.25rem',
  display: 'inline-block',
  verticalAlign: 'middle',
  margin: '0 0 1rem 0',
  padding: '0.85em 1em',
  WebkitAppearance: 'none',
  border: `2px solid ${colors.Primary}`,
  borderRadius: '10',
  lineHeight: '1',
  textAlign: 'center',
  cursor: 'pointer',
  backgroundColor: colors.White,
  color: colors.Primary
})