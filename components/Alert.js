import glamorous from 'glamorous'
import Transition from 'react-motion-ui-pack'
import { colors } from '../styles/brand'
import { hexToRgbA } from '../utils'

const Body = glamorous.div({
  position: 'absolute',
  top: 0,
  color: colors.White,
  backgroundColor: hexToRgbA(colors.Primary, 0.8),
  padding: '1rem'
})

export default ({ children, when }) => (
  <Transition
    component={false}
    enter={{
      opacity: 1
    }}
    leave={{
      opacity: 0
    }}
  >
    { when &&
      <Body key="alert">
        {children}
      </Body>
    }
  </Transition>
)