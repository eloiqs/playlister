import * as React from 'react'
import Head from 'next/head'
import FullPage from '../components/FullPage'
import BrandTitle from '../components/BrandTitle'
import LoginButton from '../components/LoginButton'
import Alert from '../components/Alert'

export default class {
  state = {
    error: false
  }

  componentWillMount () {
    const { error } = this.props.url.query
    if (error) {
      this.state.error = 'You must be connected to access these features'
      setTimeout(this.hideError, 5000)
    }
  }

  render () {
    const { error } = this.state;
    return (
      <FullPage>
        <Alert when={error}>
          <h5>{error}</h5>
        </Alert>
        <BrandTitle>Playlister</BrandTitle>
        <LoginButton onClick={this.login}>Log in with spotify</LoginButton>
      </FullPage>
    )
  }

  login () {
    window.location.href = '/login'
  }

  hideError = () => {
    this.setState(() => ({
      ...this.state,
      error: false
    }))
  }
}