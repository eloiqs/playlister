import * as React from 'react'
import 'isomorphic-fetch'
import Link from 'next/link'
import Head from 'next/head'

export default class extends React.Component<any, any> {
  static async getInitialProps({ req, query }) {
    try {
      return await getUserData(query);
    } catch (error) {
      console.log(error)
      return { error }
    }
  }

  render() {
    return (
      <div>
        <Head>
          <title>Playlister</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation-flex.css" />
        </Head>
        <h1>Logged in as {this.props.profile.display_name}</h1>
        <button onClick={() => window.location.href = '/logout'}>Log in as a different user</button>
        <div style={{clear:'both'}}>
          <pre style={{float:'left',width:'32%',overflow:'hidden'}}>{JSON.stringify({ profile: this.props.profile }, void 0, '\t')}</pre>
          <pre style={{float:'left',width:'32%',overflow:'hidden'}}>{JSON.stringify({ tracks: this.props.tracks }, void 0, '\t')}</pre>
          <pre style={{float:'left',width:'32%',overflow:'hidden'}}>{JSON.stringify({ playlists: this.props.playlists }, void 0, '\t')}</pre>
        </div>
      </div>
    )
  }
}

const getProfile = (config) => fetch('https://api.spotify.com/v1/me', config)
const getTracks = (config) => fetch('https://api.spotify.com/v1/me/tracks', config)
const getPlaylists = (config) => fetch('https://api.spotify.com/v1/me/playlists', config)

const getUserData = async ({ access_token }) => {
  const config = {
    method: 'GET',
    headers: { 'Authorization': `Bearer ${access_token}` }
  };
  try {
    const [profileRes, tracksRes, playlistRes] = await Promise.all([getProfile(config), getTracks(config), getPlaylists(config)])
    const [profile, tracks, playlists] = await Promise.all([profileRes.json(), tracksRes.json(), playlistRes.json()])
    const playlistsWithTracks = await getPlaylistsWithTracks(config, playlists)
    return { profile, tracks: getUsefulTracksData(tracks), playlists: playlistsWithTracks }
  } catch (err) {
    throw err
  }
}

const getUsefulTracksData = ({ items }) => {
  return items.map(({ track }) => ({
    artist: track.artists[0].name,
    name: track.name
  }))
}

const getPlaylistsWithTracks = (config, { items }) => {
  const promises = items.map(({ name, tracks }) => {
    return new Promise((resolve) => {
      fetch(tracks.href, config)
        .then(res => res.json())
        .then((parsedTracks:any) => {
          resolve({
            name,
            tracks: getUsefulTracksData(parsedTracks)
          })
        })
    })
  })

  return Promise.all(promises)
}