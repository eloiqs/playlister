export const hexToRgbA = (hex, alpha) => {
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    let hexDigits = hex.substring(1).split('')
    if (hexDigits.length === 3) {
        hexDigits = [
          hexDigits[0], 
          hexDigits[0], 
          hexDigits[1], 
          hexDigits[1], 
          hexDigits[2], 
          hexDigits[2]
        ]
    }
    const color = '0x' + hexDigits.join('')
    const red = (color >> 16) & 255
    const green = (color >> 8) & 255
    const blue = color & 255

    return `rgba(${red}, ${green}, ${blue}, ${alpha})`
  }
}